
# CodeForks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]



