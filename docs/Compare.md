
# Compare

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**baseCommit** | **String** |  |  [optional]
**mergeBaseCommit** | **String** |  |  [optional]
**commits** | **String** |  |  [optional]
**files** | **String** |  |  [optional]



