
# CompleteBranch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**commit** | **String** |  |  [optional]
**links** | **String** |  |  [optional]
**_protected** | **String** |  |  [optional]
**protectionUrl** | **String** |  |  [optional]



