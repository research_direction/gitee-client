
# Content

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  |  [optional]
**encoding** | **String** |  |  [optional]
**size** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**content** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**downloadUrl** | **String** |  |  [optional]
**links** | **String** |  |  [optional]



