# MilestonesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#deleteV5ReposOwnerRepoMilestonesNumber) | **DELETE** v5/repos/{owner}/{repo}/milestones/{number} | 删除项目单个里程碑
[**getV5ReposOwnerRepoMilestones**](MilestonesApi.md#getV5ReposOwnerRepoMilestones) | **GET** v5/repos/{owner}/{repo}/milestones | 获取项目所有里程碑
[**getV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#getV5ReposOwnerRepoMilestonesNumber) | **GET** v5/repos/{owner}/{repo}/milestones/{number} | 获取项目单个里程碑
[**patchV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#patchV5ReposOwnerRepoMilestonesNumber) | **PATCH** v5/repos/{owner}/{repo}/milestones/{number} | 更新项目里程碑
[**postV5ReposOwnerRepoMilestones**](MilestonesApi.md#postV5ReposOwnerRepoMilestones) | **POST** v5/repos/{owner}/{repo}/milestones | 创建项目里程碑


<a name="deleteV5ReposOwnerRepoMilestonesNumber"></a>
# **deleteV5ReposOwnerRepoMilestonesNumber**
> Void deleteV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken)

删除项目单个里程碑

删除项目单个里程碑

### Example
```java
// Import classes:
//import com.gitee.api.api.MilestonesApi;

MilestonesApi apiInstance =  new ApiClient().create(MilestonesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个里程碑，即本项目里程碑的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个里程碑，即本项目里程碑的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoMilestones"></a>
# **getV5ReposOwnerRepoMilestones**
> java.util.List&lt;Milestone&gt; getV5ReposOwnerRepoMilestones(owner, repo, accessToken, state, sort, direction, page, perPage)

获取项目所有里程碑

获取项目所有里程碑

### Example
```java
// Import classes:
//import com.gitee.api.api.MilestonesApi;

MilestonesApi apiInstance =  new ApiClient().create(MilestonesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 里程碑状态: open, closed, all。默认: open
String sort = "due_on"; // String | 排序方式: due_on
String direction = "direction_example"; // String | 升序(asc)或是降序(desc)。默认: asc
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Milestone>> result = apiInstance.getV5ReposOwnerRepoMilestones(owner, repo, accessToken, state, sort, direction, page, perPage);
result.subscribe(new Observer<java.util.List<Milestone>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Milestone> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open] [enum: open, closed, all]
 **sort** | **String**| 排序方式: due_on | [optional] [default to due_on] [enum: due_on]
 **direction** | **String**| 升序(asc)或是降序(desc)。默认: asc | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Milestone&gt;**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoMilestonesNumber"></a>
# **getV5ReposOwnerRepoMilestonesNumber**
> Milestone getV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken)

获取项目单个里程碑

获取项目单个里程碑

### Example
```java
// Import classes:
//import com.gitee.api.api.MilestonesApi;

MilestonesApi apiInstance =  new ApiClient().create(MilestonesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个里程碑，即本项目里程碑的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Milestone> result = apiInstance.getV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken);
result.subscribe(new Observer<Milestone>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Milestone response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个里程碑，即本项目里程碑的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoMilestonesNumber"></a>
# **patchV5ReposOwnerRepoMilestonesNumber**
> Milestone patchV5ReposOwnerRepoMilestonesNumber(owner, repo, number, title, accessToken, state, description, dueOn)

更新项目里程碑

更新项目里程碑

### Example
```java
// Import classes:
//import com.gitee.api.api.MilestonesApi;

MilestonesApi apiInstance =  new ApiClient().create(MilestonesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个里程碑，即本项目里程碑的序数
String title = "title_example"; // String | 里程碑标题
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 里程碑状态: open, closed, all。默认: open
String description = "description_example"; // String | 里程碑具体描述
String dueOn = "dueOn_example"; // String | 里程碑的截止日期
Observable<Milestone> result = apiInstance.patchV5ReposOwnerRepoMilestonesNumber(owner, repo, number, title, accessToken, state, description, dueOn);
result.subscribe(new Observer<Milestone>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Milestone response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个里程碑，即本项目里程碑的序数 |
 **title** | **String**| 里程碑标题 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open] [enum: open, closed, all]
 **description** | **String**| 里程碑具体描述 | [optional]
 **dueOn** | **String**| 里程碑的截止日期 | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoMilestones"></a>
# **postV5ReposOwnerRepoMilestones**
> Milestone postV5ReposOwnerRepoMilestones(owner, repo, title, accessToken, state, description, dueOn)

创建项目里程碑

创建项目里程碑

### Example
```java
// Import classes:
//import com.gitee.api.api.MilestonesApi;

MilestonesApi apiInstance =  new ApiClient().create(MilestonesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String title = "title_example"; // String | 里程碑标题
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 里程碑状态: open, closed, all。默认: open
String description = "description_example"; // String | 里程碑具体描述
String dueOn = "dueOn_example"; // String | 里程碑的截止日期
Observable<Milestone> result = apiInstance.postV5ReposOwnerRepoMilestones(owner, repo, title, accessToken, state, description, dueOn);
result.subscribe(new Observer<Milestone>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Milestone response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **title** | **String**| 里程碑标题 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open] [enum: open, closed, all]
 **description** | **String**| 里程碑具体描述 | [optional]
 **dueOn** | **String**| 里程碑的截止日期 | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

