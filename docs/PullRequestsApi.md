# PullRequestsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#deleteV5ReposOwnerRepoPullsCommentsId) | **DELETE** v5/repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
[**deleteV5ReposOwnerRepoPullsNumberRequestedReviewers**](PullRequestsApi.md#deleteV5ReposOwnerRepoPullsNumberRequestedReviewers) | **DELETE** v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 移除审查人员
[**getV5ReposOwnerRepoPulls**](PullRequestsApi.md#getV5ReposOwnerRepoPulls) | **GET** v5/repos/{owner}/{repo}/pulls | 获取Pull Request列表
[**getV5ReposOwnerRepoPullsComments**](PullRequestsApi.md#getV5ReposOwnerRepoPullsComments) | **GET** v5/repos/{owner}/{repo}/pulls/comments | 获取该项目下的所有Pull Request评论
[**getV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#getV5ReposOwnerRepoPullsCommentsId) | **GET** v5/repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
[**getV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumber) | **GET** v5/repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
[**getV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberComments) | **GET** v5/repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
[**getV5ReposOwnerRepoPullsNumberCommits**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberCommits) | **GET** v5/repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
[**getV5ReposOwnerRepoPullsNumberFiles**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberFiles) | **GET** v5/repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
[**getV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberMerge) | **GET** v5/repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
[**getV5ReposOwnerRepoPullsNumberRequestedReviewers**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberRequestedReviewers) | **GET** v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 获取审查人员的列表
[**patchV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#patchV5ReposOwnerRepoPullsCommentsId) | **PATCH** v5/repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
[**patchV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#patchV5ReposOwnerRepoPullsNumber) | **PATCH** v5/repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
[**postV5ReposOwnerRepoPulls**](PullRequestsApi.md#postV5ReposOwnerRepoPulls) | **POST** v5/repos/{owner}/{repo}/pulls | 创建Pull Request
[**postV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberComments) | **POST** v5/repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
[**postV5ReposOwnerRepoPullsNumberRequestedReviewers**](PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberRequestedReviewers) | **POST** v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 增加审查人员
[**putV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#putV5ReposOwnerRepoPullsNumberMerge) | **PUT** v5/repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request


<a name="deleteV5ReposOwnerRepoPullsCommentsId"></a>
# **deleteV5ReposOwnerRepoPullsCommentsId**
> Void deleteV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken)

删除评论

删除评论

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoPullsNumberRequestedReviewers"></a>
# **deleteV5ReposOwnerRepoPullsNumberRequestedReviewers**
> Void deleteV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, accessToken)

移除审查人员

移除审查人员

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
java.util.List<String> reviewers = Arrays.asList("reviewers_example"); // java.util.List<String> | 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对username换行即可
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **reviewers** | [**java.util.List&lt;String&gt;**](String.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对username换行即可 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPulls"></a>
# **getV5ReposOwnerRepoPulls**
> java.util.List&lt;PullRequest&gt; getV5ReposOwnerRepoPulls(owner, repo, accessToken, state, head, base, sort, direction, page, perPage)

获取Pull Request列表

获取Pull Request列表

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 可选。Pull Request 状态
String head = "head_example"; // String | 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch
String base = "base_example"; // String | 可选。Pull Request 提交目标分支的名称。
String sort = "created"; // String | 可选。排序字段，默认按创建时间
String direction = "desc"; // String | 可选。升序/降序
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<PullRequest>> result = apiInstance.getV5ReposOwnerRepoPulls(owner, repo, accessToken, state, head, base, sort, direction, page, perPage);
result.subscribe(new Observer<java.util.List<PullRequest>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<PullRequest> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 可选。Pull Request 状态 | [optional] [default to open] [enum: open, closed, all]
 **head** | **String**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | [optional]
 **base** | **String**| 可选。Pull Request 提交目标分支的名称。 | [optional]
 **sort** | **String**| 可选。排序字段，默认按创建时间 | [optional] [default to created] [enum: created, updated, popularity, long-running]
 **direction** | **String**| 可选。升序/降序 | [optional] [default to desc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;PullRequest&gt;**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsComments"></a>
# **getV5ReposOwnerRepoPullsComments**
> java.util.List&lt;PullRequestComments&gt; getV5ReposOwnerRepoPullsComments(owner, repo, accessToken, sort, direction, since, page, perPage)

获取该项目下的所有Pull Request评论

获取该项目下的所有Pull Request评论

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | 可选。按创建时间/更新时间排序
String direction = "desc"; // String | 可选。升序/降序
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<PullRequestComments>> result = apiInstance.getV5ReposOwnerRepoPullsComments(owner, repo, accessToken, sort, direction, since, page, perPage);
result.subscribe(new Observer<java.util.List<PullRequestComments>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<PullRequestComments> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 可选。按创建时间/更新时间排序 | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 可选。升序/降序 | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;PullRequestComments&gt;**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsCommentsId"></a>
# **getV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments getV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken)

获取Pull Request的某个评论

获取Pull Request的某个评论

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<PullRequestComments> result = apiInstance.getV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken);
result.subscribe(new Observer<PullRequestComments>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(PullRequestComments response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumber"></a>
# **getV5ReposOwnerRepoPullsNumber**
> PullRequest getV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken)

获取单个Pull Request

获取单个Pull Request

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<PullRequest> result = apiInstance.getV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken);
result.subscribe(new Observer<PullRequest>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(PullRequest response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberComments"></a>
# **getV5ReposOwnerRepoPullsNumberComments**
> java.util.List&lt;PullRequestComments&gt; getV5ReposOwnerRepoPullsNumberComments(owner, repo, number, accessToken, page, perPage)

获取某个Pull Request的所有评论

获取某个Pull Request的所有评论

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<PullRequestComments>> result = apiInstance.getV5ReposOwnerRepoPullsNumberComments(owner, repo, number, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<PullRequestComments>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<PullRequestComments> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;PullRequestComments&gt;**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberCommits"></a>
# **getV5ReposOwnerRepoPullsNumberCommits**
> java.util.List&lt;PullRequestCommits&gt; getV5ReposOwnerRepoPullsNumberCommits(owner, repo, number, accessToken)

获取某Pull Request的所有Commit信息。最多显示250条Commit

获取某Pull Request的所有Commit信息。最多显示250条Commit

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<java.util.List<PullRequestCommits>> result = apiInstance.getV5ReposOwnerRepoPullsNumberCommits(owner, repo, number, accessToken);
result.subscribe(new Observer<java.util.List<PullRequestCommits>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<PullRequestCommits> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**java.util.List&lt;PullRequestCommits&gt;**](PullRequestCommits.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberFiles"></a>
# **getV5ReposOwnerRepoPullsNumberFiles**
> java.util.List&lt;PullRequestFiles&gt; getV5ReposOwnerRepoPullsNumberFiles(owner, repo, number, accessToken)

Pull Request Commit文件列表。最多显示300条diff

Pull Request Commit文件列表。最多显示300条diff

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<java.util.List<PullRequestFiles>> result = apiInstance.getV5ReposOwnerRepoPullsNumberFiles(owner, repo, number, accessToken);
result.subscribe(new Observer<java.util.List<PullRequestFiles>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<PullRequestFiles> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**java.util.List&lt;PullRequestFiles&gt;**](PullRequestFiles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberMerge"></a>
# **getV5ReposOwnerRepoPullsNumberMerge**
> Void getV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken)

判断Pull Request是否已经合并

判断Pull Request是否已经合并

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberRequestedReviewers"></a>
# **getV5ReposOwnerRepoPullsNumberRequestedReviewers**
> UserBasic getV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, accessToken, page, perPage)

获取审查人员的列表

获取审查人员的列表

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<UserBasic> result = apiInstance.getV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, accessToken, page, perPage);
result.subscribe(new Observer<UserBasic>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserBasic response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoPullsCommentsId"></a>
# **patchV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments patchV5ReposOwnerRepoPullsCommentsId(owner, repo, id, body, accessToken)

编辑评论

编辑评论

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String body = "body_example"; // String | 必填。评论内容
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<PullRequestComments> result = apiInstance.patchV5ReposOwnerRepoPullsCommentsId(owner, repo, id, body, accessToken);
result.subscribe(new Observer<PullRequestComments>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(PullRequestComments response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | **String**| 必填。评论内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoPullsNumber"></a>
# **patchV5ReposOwnerRepoPullsNumber**
> PullRequest patchV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken, title, body, state)

更新Pull Request信息

更新Pull Request信息

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
String title = "title_example"; // String | 可选。Pull Request 标题
String body = "body_example"; // String | 可选。Pull Request 内容
String state = "state_example"; // String | 可选。Pull Request 状态
Observable<PullRequest> result = apiInstance.patchV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken, title, body, state);
result.subscribe(new Observer<PullRequest>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(PullRequest response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **title** | **String**| 可选。Pull Request 标题 | [optional]
 **body** | **String**| 可选。Pull Request 内容 | [optional]
 **state** | **String**| 可选。Pull Request 状态 | [optional] [enum: open, closed]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPulls"></a>
# **postV5ReposOwnerRepoPulls**
> PullRequest postV5ReposOwnerRepoPulls(owner, repo, title, head, base, accessToken, body, issue)

创建Pull Request

创建Pull Request

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String title = "title_example"; // String | 必填。Pull Request 标题
String head = "head_example"; // String | 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch
String base = "base_example"; // String | 必填。Pull Request 提交目标分支的名称
String accessToken = "accessToken_example"; // String | 用户授权码
String body = "body_example"; // String | 可选。Pull Request 内容
String issue = "issue_example"; // String | 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充
Observable<PullRequest> result = apiInstance.postV5ReposOwnerRepoPulls(owner, repo, title, head, base, accessToken, body, issue);
result.subscribe(new Observer<PullRequest>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(PullRequest response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **title** | **String**| 必填。Pull Request 标题 |
 **head** | **String**| 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch |
 **base** | **String**| 必填。Pull Request 提交目标分支的名称 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **body** | **String**| 可选。Pull Request 内容 | [optional]
 **issue** | **String**| 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPullsNumberComments"></a>
# **postV5ReposOwnerRepoPullsNumberComments**
> PullRequestComments postV5ReposOwnerRepoPullsNumberComments(owner, repo, number, body, accessToken, commitId, path, position)

提交Pull Request评论

提交Pull Request评论

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String body = "body_example"; // String | 必填。评论内容
String accessToken = "accessToken_example"; // String | 用户授权码
String commitId = "commitId_example"; // String | 可选。PR代码评论的commit id
String path = "path_example"; // String | 可选。PR代码评论的文件名
Integer position = 56; // Integer | 可选。PR代码评论diff中的行数
Observable<PullRequestComments> result = apiInstance.postV5ReposOwnerRepoPullsNumberComments(owner, repo, number, body, accessToken, commitId, path, position);
result.subscribe(new Observer<PullRequestComments>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(PullRequestComments response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **body** | **String**| 必填。评论内容 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **commitId** | **String**| 可选。PR代码评论的commit id | [optional]
 **path** | **String**| 可选。PR代码评论的文件名 | [optional]
 **position** | **Integer**| 可选。PR代码评论diff中的行数 | [optional]

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPullsNumberRequestedReviewers"></a>
# **postV5ReposOwnerRepoPullsNumberRequestedReviewers**
> PullRequest postV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, accessToken)

增加审查人员

增加审查人员

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
java.util.List<String> reviewers = Arrays.asList("reviewers_example"); // java.util.List<String> | 审核人员的username，数组形式: [\"oschina\", \"tom\"], 此处试验直接对标签名换行即可
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<PullRequest> result = apiInstance.postV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, accessToken);
result.subscribe(new Observer<PullRequest>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(PullRequest response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **reviewers** | [**java.util.List&lt;String&gt;**](String.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对标签名换行即可 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoPullsNumberMerge"></a>
# **putV5ReposOwnerRepoPullsNumberMerge**
> Void putV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken)

合并Pull Request

合并Pull Request

### Example
```java
// Import classes:
//import com.gitee.api.api.PullRequestsApi;

PullRequestsApi apiInstance =  new ApiClient().create(PullRequestsApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer number = 56; // Integer | 第几个PR，即本项目PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **number** | **Integer**| 第几个PR，即本项目PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

