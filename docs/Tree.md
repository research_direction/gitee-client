
# Tree

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**tree** | **String** |  |  [optional]
**truncated** | **String** |  |  [optional]



