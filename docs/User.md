
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**login** | **String** |  |  [optional]
**id** | **Integer** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**followersUrl** | **String** |  |  [optional]
**followingUrl** | **String** |  |  [optional]
**gistsUrl** | **String** |  |  [optional]
**starredUrl** | **String** |  |  [optional]
**subscriptionsUrl** | **String** |  |  [optional]
**organizationsUrl** | **String** |  |  [optional]
**reposUrl** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**receivedEventsUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**siteAdmin** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**blog** | **String** |  |  [optional]
**weibo** | **String** |  |  [optional]
**bio** | **String** |  |  [optional]
**publicRepos** | **String** |  |  [optional]
**publicGists** | **String** |  |  [optional]
**followers** | **String** |  |  [optional]
**following** | **String** |  |  [optional]
**stared** | **String** |  |  [optional]
**watched** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]



