# UsersApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5UserFollowingUsername**](UsersApi.md#deleteV5UserFollowingUsername) | **DELETE** v5/user/following/{username} | 取消关注一个用户
[**deleteV5UserKeysId**](UsersApi.md#deleteV5UserKeysId) | **DELETE** v5/user/keys/{id} | 删除一个公钥
[**deleteV5UserUnconfirmedEmail**](UsersApi.md#deleteV5UserUnconfirmedEmail) | **DELETE** v5/user/unconfirmed_email | 删除授权用户未激活的邮箱地址
[**getV5User**](UsersApi.md#getV5User) | **GET** v5/user | 获取授权用户的资料
[**getV5UserAddress**](UsersApi.md#getV5UserAddress) | **GET** v5/user/address | 获取授权用户的地理信息
[**getV5UserEmails**](UsersApi.md#getV5UserEmails) | **GET** v5/user/emails | 获取授权用户的邮箱地址
[**getV5UserFollowers**](UsersApi.md#getV5UserFollowers) | **GET** v5/user/followers | 列出授权用户的关注者
[**getV5UserFollowing**](UsersApi.md#getV5UserFollowing) | **GET** v5/user/following | 列出授权用户正关注的用户
[**getV5UserFollowingUsername**](UsersApi.md#getV5UserFollowingUsername) | **GET** v5/user/following/{username} | 检查授权用户是否关注了一个用户
[**getV5UserKeys**](UsersApi.md#getV5UserKeys) | **GET** v5/user/keys | 列出授权用户的所有公钥
[**getV5UserKeysId**](UsersApi.md#getV5UserKeysId) | **GET** v5/user/keys/{id} | 获取一个公钥
[**getV5UsersUsername**](UsersApi.md#getV5UsersUsername) | **GET** v5/users/{username} | 获取一个用户
[**getV5UsersUsernameFollowers**](UsersApi.md#getV5UsersUsernameFollowers) | **GET** v5/users/{username}/followers | 列出指定用户的关注者
[**getV5UsersUsernameFollowing**](UsersApi.md#getV5UsersUsernameFollowing) | **GET** v5/users/{username}/following | 列出指定用户正在关注的用户
[**getV5UsersUsernameFollowingTargetUser**](UsersApi.md#getV5UsersUsernameFollowingTargetUser) | **GET** v5/users/{username}/following/{target_user} | 检查指定用户是否关注目标用户
[**getV5UsersUsernameKeys**](UsersApi.md#getV5UsersUsernameKeys) | **GET** v5/users/{username}/keys | 列出指定用户的所有公钥
[**patchV5User**](UsersApi.md#patchV5User) | **PATCH** v5/user | 更新授权用户的资料
[**patchV5UserAddress**](UsersApi.md#patchV5UserAddress) | **PATCH** v5/user/address | 更新授权用户的地理信息
[**postV5UserEmails**](UsersApi.md#postV5UserEmails) | **POST** v5/user/emails | 添加授权用户的新邮箱地址
[**postV5UserKeys**](UsersApi.md#postV5UserKeys) | **POST** v5/user/keys | 添加一个公钥
[**putV5UserFollowingUsername**](UsersApi.md#putV5UserFollowingUsername) | **PUT** v5/user/following/{username} | 关注一个用户


<a name="deleteV5UserFollowingUsername"></a>
# **deleteV5UserFollowingUsername**
> Void deleteV5UserFollowingUsername(username, accessToken)

取消关注一个用户

取消关注一个用户

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5UserFollowingUsername(username, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5UserKeysId"></a>
# **deleteV5UserKeysId**
> Void deleteV5UserKeysId(id, accessToken)

删除一个公钥

删除一个公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5UserKeysId(id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5UserUnconfirmedEmail"></a>
# **deleteV5UserUnconfirmedEmail**
> Void deleteV5UserUnconfirmedEmail(accessToken)

删除授权用户未激活的邮箱地址

删除授权用户未激活的邮箱地址

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5UserUnconfirmedEmail(accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5User"></a>
# **getV5User**
> UserDetail getV5User(accessToken)

获取授权用户的资料

获取授权用户的资料

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<UserDetail> result = apiInstance.getV5User(accessToken);
result.subscribe(new Observer<UserDetail>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserDetail response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserDetail**](UserDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserAddress"></a>
# **getV5UserAddress**
> UserAddress getV5UserAddress(accessToken)

获取授权用户的地理信息

获取授权用户的地理信息

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<UserAddress> result = apiInstance.getV5UserAddress(accessToken);
result.subscribe(new Observer<UserAddress>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserAddress response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserAddress**](UserAddress.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserEmails"></a>
# **getV5UserEmails**
> UserEmail getV5UserEmails(accessToken)

获取授权用户的邮箱地址

获取授权用户的邮箱地址

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<UserEmail> result = apiInstance.getV5UserEmails(accessToken);
result.subscribe(new Observer<UserEmail>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserEmail response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserEmail**](UserEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserFollowers"></a>
# **getV5UserFollowers**
> java.util.List&lt;UserBasic&gt; getV5UserFollowers(accessToken, page, perPage)

列出授权用户的关注者

列出授权用户的关注者

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserBasic>> result = apiInstance.getV5UserFollowers(accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<UserBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserFollowing"></a>
# **getV5UserFollowing**
> java.util.List&lt;UserBasic&gt; getV5UserFollowing(accessToken, page, perPage)

列出授权用户正关注的用户

列出授权用户正关注的用户

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserBasic>> result = apiInstance.getV5UserFollowing(accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<UserBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserFollowingUsername"></a>
# **getV5UserFollowingUsername**
> Void getV5UserFollowingUsername(username, accessToken)

检查授权用户是否关注了一个用户

检查授权用户是否关注了一个用户

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5UserFollowingUsername(username, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserKeys"></a>
# **getV5UserKeys**
> java.util.List&lt;SSHKey&gt; getV5UserKeys(accessToken, page, perPage)

列出授权用户的所有公钥

列出授权用户的所有公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<SSHKey>> result = apiInstance.getV5UserKeys(accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<SSHKey>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<SSHKey> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;SSHKey&gt;**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserKeysId"></a>
# **getV5UserKeysId**
> SSHKey getV5UserKeysId(id, accessToken)

获取一个公钥

获取一个公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<SSHKey> result = apiInstance.getV5UserKeysId(id, accessToken);
result.subscribe(new Observer<SSHKey>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(SSHKey response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsername"></a>
# **getV5UsersUsername**
> User getV5UsersUsername(username, accessToken)

获取一个用户

获取一个用户

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<User> result = apiInstance.getV5UsersUsername(username, accessToken);
result.subscribe(new Observer<User>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(User response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameFollowers"></a>
# **getV5UsersUsernameFollowers**
> java.util.List&lt;UserBasic&gt; getV5UsersUsernameFollowers(username, accessToken, page, perPage)

列出指定用户的关注者

列出指定用户的关注者

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserBasic>> result = apiInstance.getV5UsersUsernameFollowers(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<UserBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameFollowing"></a>
# **getV5UsersUsernameFollowing**
> java.util.List&lt;UserBasic&gt; getV5UsersUsernameFollowing(username, accessToken, page, perPage)

列出指定用户正在关注的用户

列出指定用户正在关注的用户

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<UserBasic>> result = apiInstance.getV5UsersUsernameFollowing(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<UserBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<UserBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameFollowingTargetUser"></a>
# **getV5UsersUsernameFollowingTargetUser**
> Void getV5UsersUsernameFollowingTargetUser(username, targetUser, accessToken)

检查指定用户是否关注目标用户

检查指定用户是否关注目标用户

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String targetUser = "targetUser_example"; // String | 目标用户的用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5UsersUsernameFollowingTargetUser(username, targetUser, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **targetUser** | **String**| 目标用户的用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameKeys"></a>
# **getV5UsersUsernameKeys**
> java.util.List&lt;SSHKeyBasic&gt; getV5UsersUsernameKeys(username, accessToken, page, perPage)

列出指定用户的所有公钥

列出指定用户的所有公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<SSHKeyBasic>> result = apiInstance.getV5UsersUsernameKeys(username, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<SSHKeyBasic>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<SSHKeyBasic> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;SSHKeyBasic&gt;**](SSHKeyBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5User"></a>
# **patchV5User**
> UserDetail patchV5User(accessToken, name, blog, weibo, bio)

更新授权用户的资料

更新授权用户的资料

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
String name = "name_example"; // String | 昵称
String blog = "blog_example"; // String | 微博链接
String weibo = "weibo_example"; // String | 博客站点
String bio = "bio_example"; // String | 自我介绍
Observable<UserDetail> result = apiInstance.patchV5User(accessToken, name, blog, weibo, bio);
result.subscribe(new Observer<UserDetail>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserDetail response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **name** | **String**| 昵称 | [optional]
 **blog** | **String**| 微博链接 | [optional]
 **weibo** | **String**| 博客站点 | [optional]
 **bio** | **String**| 自我介绍 | [optional]

### Return type

[**UserDetail**](UserDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5UserAddress"></a>
# **patchV5UserAddress**
> UserDetail patchV5UserAddress(accessToken, name, tel, address, province, city, zipCode, comment)

更新授权用户的地理信息

更新授权用户的地理信息

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
String name = "name_example"; // String | 联系人名
String tel = "tel_example"; // String | 联系电话
String address = "address_example"; // String | 联系地址
String province = "province_example"; // String | 省份
String city = "city_example"; // String | 城市
String zipCode = "zipCode_example"; // String | 邮政编码
String comment = "comment_example"; // String | 备注
Observable<UserDetail> result = apiInstance.patchV5UserAddress(accessToken, name, tel, address, province, city, zipCode, comment);
result.subscribe(new Observer<UserDetail>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserDetail response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **name** | **String**| 联系人名 | [optional]
 **tel** | **String**| 联系电话 | [optional]
 **address** | **String**| 联系地址 | [optional]
 **province** | **String**| 省份 | [optional]
 **city** | **String**| 城市 | [optional]
 **zipCode** | **String**| 邮政编码 | [optional]
 **comment** | **String**| 备注 | [optional]

### Return type

[**UserDetail**](UserDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5UserEmails"></a>
# **postV5UserEmails**
> UserEmail postV5UserEmails(email, accessToken)

添加授权用户的新邮箱地址

添加授权用户的新邮箱地址

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String email = "email_example"; // String | 新的邮箱地址
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<UserEmail> result = apiInstance.postV5UserEmails(email, accessToken);
result.subscribe(new Observer<UserEmail>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(UserEmail response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| 新的邮箱地址 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserEmail**](UserEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5UserKeys"></a>
# **postV5UserKeys**
> SSHKey postV5UserKeys(key, title, accessToken)

添加一个公钥

添加一个公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String key = "key_example"; // String | 公钥内容.
String title = "title_example"; // String | 公钥名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<SSHKey> result = apiInstance.postV5UserKeys(key, title, accessToken);
result.subscribe(new Observer<SSHKey>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(SSHKey response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| 公钥内容. |
 **title** | **String**| 公钥名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5UserFollowingUsername"></a>
# **putV5UserFollowingUsername**
> Void putV5UserFollowingUsername(username, accessToken)

关注一个用户

关注一个用户

### Example
```java
// Import classes:
//import com.gitee.api.api.UsersApi;

UsersApi apiInstance =  new ApiClient().create(UsersApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5UserFollowingUsername(username, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

