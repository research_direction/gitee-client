package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.Milestone;


public interface MilestonesApi {
  /**
   * 删除项目单个里程碑
   * 删除项目单个里程碑
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个里程碑，即本项目里程碑的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/repos/{owner}/{repo}/milestones/{number}")
  Observable<Void> deleteV5ReposOwnerRepoMilestonesNumber(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取项目所有里程碑
   * 获取项目所有里程碑
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param state 里程碑状态: open, closed, all。默认: open (optional, default to open)
   * @param sort 排序方式: due_on (optional, default to due_on)
   * @param direction 升序(asc)或是降序(desc)。默认: asc (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Milestone&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/milestones")
  Observable<java.util.List<Milestone>> getV5ReposOwnerRepoMilestones(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("state") String state, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取项目单个里程碑
   * 获取项目单个里程碑
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个里程碑，即本项目里程碑的序数 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Milestone&gt;
   */
  @GET("v5/repos/{owner}/{repo}/milestones/{number}")
  Observable<Milestone> getV5ReposOwnerRepoMilestonesNumber(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 更新项目里程碑
   * 更新项目里程碑
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param number 第几个里程碑，即本项目里程碑的序数 (required)
   * @param title 里程碑标题 (required)
   * @param accessToken 用户授权码 (optional)
   * @param state 里程碑状态: open, closed, all。默认: open (optional, default to open)
   * @param description 里程碑具体描述 (optional)
   * @param dueOn 里程碑的截止日期 (optional)
   * @return Call&lt;Milestone&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/repos/{owner}/{repo}/milestones/{number}")
  Observable<Milestone> patchV5ReposOwnerRepoMilestonesNumber(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("number") Integer number, @retrofit2.http.Field("title") String title, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("state") String state, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("due_on") String dueOn
  );

  /**
   * 创建项目里程碑
   * 创建项目里程碑
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param title 里程碑标题 (required)
   * @param accessToken 用户授权码 (optional)
   * @param state 里程碑状态: open, closed, all。默认: open (optional, default to open)
   * @param description 里程碑具体描述 (optional)
   * @param dueOn 里程碑的截止日期 (optional)
   * @return Call&lt;Milestone&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/milestones")
  Observable<Milestone> postV5ReposOwnerRepoMilestones(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("title") String title, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("state") String state, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("due_on") String dueOn
  );

}
