package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * EventPayloadCommits
 */

public class EventPayloadCommits   {
  @SerializedName("sha")
  private String sha = null;

  @SerializedName("author")
  private EventPayloadAuthor author = null;

  @SerializedName("message")
  private String message = null;

  @SerializedName("url")
  private String url = null;

  public EventPayloadCommits sha(String sha) {
    this.sha = sha;
    return this;
  }

   /**
   * Get sha
   * @return sha
  **/
  public String getSha() {
    return sha;
  }

  public void setSha(String sha) {
    this.sha = sha;
  }

  public EventPayloadCommits author(EventPayloadAuthor author) {
    this.author = author;
    return this;
  }

   /**
   * Get author
   * @return author
  **/
  public EventPayloadAuthor getAuthor() {
    return author;
  }

  public void setAuthor(EventPayloadAuthor author) {
    this.author = author;
  }

  public EventPayloadCommits message(String message) {
    this.message = message;
    return this;
  }

   /**
   * Get message
   * @return message
  **/
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public EventPayloadCommits url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventPayloadCommits eventPayloadCommits = (EventPayloadCommits) o;
    return Objects.equals(this.sha, eventPayloadCommits.sha) &&
        Objects.equals(this.author, eventPayloadCommits.author) &&
        Objects.equals(this.message, eventPayloadCommits.message) &&
        Objects.equals(this.url, eventPayloadCommits.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sha, author, message, url);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventPayloadCommits {\n");
    
    sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
    sb.append("    author: ").append(toIndentedString(author)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

