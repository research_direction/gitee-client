package com.gitee.api.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * this is description
 */
@ApiModel(description = "this is description")

@javax.annotation.Generated(value = "io.swagger.generator.online.JavaClientCodegenImpl", date = "2017-11-16T14:49:03.082+08:00")

public class Hook   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("created_at")
  private java.util.Date createdAt = null;

  @SerializedName("password")
  private String password = null;

  @SerializedName("project_id")
  private Integer projectId = null;

  @SerializedName("push_events")
  private Boolean pushEvents = null;

  @SerializedName("tag_push_events")
  private Boolean tagPushEvents = null;

  @SerializedName("issues_events")
  private Boolean issuesEvents = null;

  @SerializedName("note_events")
  private Boolean noteEvents = null;

  @SerializedName("merge_requests_events")
  private Boolean mergeRequestsEvents = null;

  public Hook id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "43611", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Hook url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "http://docker.500px.me:10020/gitee", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Hook createdAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(example = "2017-11-16T14:47:30+08:00", value = "")
  public java.util.Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
  }

  public Hook password(String password) {
    this.password = password;
    return this;
  }

   /**
   * Get password
   * @return password
  **/
  @ApiModelProperty(example = "123456", value = "")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Hook projectId(Integer projectId) {
    this.projectId = projectId;
    return this;
  }

   /**
   * Get projectId
   * @return projectId
  **/
  @ApiModelProperty(example = "846452", value = "")
  public Integer getProjectId() {
    return projectId;
  }

  public void setProjectId(Integer projectId) {
    this.projectId = projectId;
  }

  public Hook pushEvents(Boolean pushEvents) {
    this.pushEvents = pushEvents;
    return this;
  }

   /**
   * Get pushEvents
   * @return pushEvents
  **/
  @ApiModelProperty(value = "")
  public Boolean isPushEvents() {
    return pushEvents;
  }

  public void setPushEvents(Boolean pushEvents) {
    this.pushEvents = pushEvents;
  }

  public Hook tagPushEvents(Boolean tagPushEvents) {
    this.tagPushEvents = tagPushEvents;
    return this;
  }

   /**
   * Get tagPushEvents
   * @return tagPushEvents
  **/
  @ApiModelProperty(value = "")
  public Boolean isTagPushEvents() {
    return tagPushEvents;
  }

  public void setTagPushEvents(Boolean tagPushEvents) {
    this.tagPushEvents = tagPushEvents;
  }

  public Hook issuesEvents(Boolean issuesEvents) {
    this.issuesEvents = issuesEvents;
    return this;
  }

   /**
   * Get issuesEvents
   * @return issuesEvents
  **/
  @ApiModelProperty(value = "")
  public Boolean isIssuesEvents() {
    return issuesEvents;
  }

  public void setIssuesEvents(Boolean issuesEvents) {
    this.issuesEvents = issuesEvents;
  }

  public Hook noteEvents(Boolean noteEvents) {
    this.noteEvents = noteEvents;
    return this;
  }

   /**
   * Get noteEvents
   * @return noteEvents
  **/
  @ApiModelProperty(value = "")
  public Boolean isNoteEvents() {
    return noteEvents;
  }

  public void setNoteEvents(Boolean noteEvents) {
    this.noteEvents = noteEvents;
  }

  public Hook mergeRequestsEvents(Boolean mergeRequestsEvents) {
    this.mergeRequestsEvents = mergeRequestsEvents;
    return this;
  }

   /**
   * Get mergeRequestsEvents
   * @return mergeRequestsEvents
  **/
  @ApiModelProperty(value = "")
  public Boolean isMergeRequestsEvents() {
    return mergeRequestsEvents;
  }

  public void setMergeRequestsEvents(Boolean mergeRequestsEvents) {
    this.mergeRequestsEvents = mergeRequestsEvents;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Hook hook = (Hook) o;
    return Objects.equals(this.id, hook.id) &&
        Objects.equals(this.url, hook.url) &&
        Objects.equals(this.createdAt, hook.createdAt) &&
        Objects.equals(this.password, hook.password) &&
        Objects.equals(this.projectId, hook.projectId) &&
        Objects.equals(this.pushEvents, hook.pushEvents) &&
        Objects.equals(this.tagPushEvents, hook.tagPushEvents) &&
        Objects.equals(this.issuesEvents, hook.issuesEvents) &&
        Objects.equals(this.noteEvents, hook.noteEvents) &&
        Objects.equals(this.mergeRequestsEvents, hook.mergeRequestsEvents);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, url, createdAt, password, projectId, pushEvents, tagPushEvents, issuesEvents, noteEvents, mergeRequestsEvents);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Hook {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    projectId: ").append(toIndentedString(projectId)).append("\n");
    sb.append("    pushEvents: ").append(toIndentedString(pushEvents)).append("\n");
    sb.append("    tagPushEvents: ").append(toIndentedString(tagPushEvents)).append("\n");
    sb.append("    issuesEvents: ").append(toIndentedString(issuesEvents)).append("\n");
    sb.append("    noteEvents: ").append(toIndentedString(noteEvents)).append("\n");
    sb.append("    mergeRequestsEvents: ").append(toIndentedString(mergeRequestsEvents)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

