package com.gitee.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * created by wuyu on 2017/11/16
 */
public class RepositoriesContribution {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("contributions")
    @Expose
    private Long contributions;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getContributions() {
        return contributions;
    }

    public void setContributions(Long contributions) {
        this.contributions = contributions;
    }

    @Override
    public String toString() {
        return "RepositoriesContribution{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", contributions=" + contributions +
                '}';
    }
}
