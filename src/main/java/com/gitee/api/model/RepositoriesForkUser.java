package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
public class RepositoriesForkUser   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("full_name")
  private String fullName = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("path")
  private String path = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("owner")
  private RepositoriesForkUserOwner owner = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("private")
  private Boolean _private = null;

  @SerializedName("fork")
  private Boolean fork = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("forks_url")
  private String forksUrl = null;

  @SerializedName("keys_url")
  private String keysUrl = null;

  @SerializedName("collaborators_url")
  private String collaboratorsUrl = null;

  @SerializedName("hooks_url")
  private String hooksUrl = null;

  @SerializedName("branches_url")
  private String branchesUrl = null;

  @SerializedName("tags_url")
  private String tagsUrl = null;

  @SerializedName("blobs_url")
  private String blobsUrl = null;

  @SerializedName("stargazers_url")
  private String stargazersUrl = null;

  @SerializedName("contributors_url")
  private String contributorsUrl = null;

  @SerializedName("commits_url")
  private String commitsUrl = null;

  @SerializedName("comments_url")
  private String commentsUrl = null;

  @SerializedName("issue_comment_url")
  private String issueCommentUrl = null;

  @SerializedName("issues_url")
  private String issuesUrl = null;

  @SerializedName("pulls_url")
  private String pullsUrl = null;

  @SerializedName("milestones_url")
  private String milestonesUrl = null;

  @SerializedName("notifications_url")
  private String notificationsUrl = null;

  @SerializedName("labels_url")
  private String labelsUrl = null;

  @SerializedName("releases_url")
  private String releasesUrl = null;

  @SerializedName("recommend")
  private Boolean recommend = null;

  @SerializedName("homepage")
  private String homepage = null;

  @SerializedName("language")
  private String language = null;

  @SerializedName("forks_count")
  private Integer forksCount = null;

  @SerializedName("stargazers_count")
  private Integer stargazersCount = null;

  @SerializedName("watchers_count")
  private Integer watchersCount = null;

  @SerializedName("default_branch")
  private String defaultBranch = null;

  @SerializedName("open_issues_count")
  private Integer openIssuesCount = null;

  @SerializedName("has_issues")
  private Boolean hasIssues = null;

  @SerializedName("has_wiki")
  private Boolean hasWiki = null;

  @SerializedName("pull_requests_enabled")
  private Boolean pullRequestsEnabled = null;

  @SerializedName("has_page")
  private Boolean hasPage = null;

  @SerializedName("pushed_at")
  private java.util.Date pushedAt = null;

  @SerializedName("created_at")
  private java.util.Date createdAt = null;

  @SerializedName("updated_at")
  private java.util.Date updatedAt = null;

  @SerializedName("parent")
  private RepositoriesForkUserParent parent = null;

  public RepositoriesForkUser id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "2688131", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public RepositoriesForkUser fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

   /**
   * Get fullName
   * @return fullName
  **/
  @ApiModelProperty(example = "fnet/gitee-client", value = "")
  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public RepositoriesForkUser url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public RepositoriesForkUser path(String path) {
    this.path = path;
    return this;
  }

   /**
   * Get path
   * @return path
  **/
  @ApiModelProperty(example = "gitee-client", value = "")
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public RepositoriesForkUser name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "gitee-client", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public RepositoriesForkUser owner(RepositoriesForkUserOwner owner) {
    this.owner = owner;
    return this;
  }

   /**
   * Get owner
   * @return owner
  **/
  @ApiModelProperty(value = "")
  public RepositoriesForkUserOwner getOwner() {
    return owner;
  }

  public void setOwner(RepositoriesForkUserOwner owner) {
    this.owner = owner;
  }

  public RepositoriesForkUser description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(example = "Gitee Java Api", value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public RepositoriesForkUser _private(Boolean _private) {
    this._private = _private;
    return this;
  }

   /**
   * Get _private
   * @return _private
  **/
  @ApiModelProperty(value = "")
  public Boolean isPrivate() {
    return _private;
  }

  public void setPrivate(Boolean _private) {
    this._private = _private;
  }

  public RepositoriesForkUser fork(Boolean fork) {
    this.fork = fork;
    return this;
  }

   /**
   * Get fork
   * @return fork
  **/
  @ApiModelProperty(value = "")
  public Boolean isFork() {
    return fork;
  }

  public void setFork(Boolean fork) {
    this.fork = fork;
  }

  public RepositoriesForkUser htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/fnet/gitee-client", value = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public RepositoriesForkUser forksUrl(String forksUrl) {
    this.forksUrl = forksUrl;
    return this;
  }

   /**
   * Get forksUrl
   * @return forksUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/forks", value = "")
  public String getForksUrl() {
    return forksUrl;
  }

  public void setForksUrl(String forksUrl) {
    this.forksUrl = forksUrl;
  }

  public RepositoriesForkUser keysUrl(String keysUrl) {
    this.keysUrl = keysUrl;
    return this;
  }

   /**
   * Get keysUrl
   * @return keysUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/keys{/key_id}", value = "")
  public String getKeysUrl() {
    return keysUrl;
  }

  public void setKeysUrl(String keysUrl) {
    this.keysUrl = keysUrl;
  }

  public RepositoriesForkUser collaboratorsUrl(String collaboratorsUrl) {
    this.collaboratorsUrl = collaboratorsUrl;
    return this;
  }

   /**
   * Get collaboratorsUrl
   * @return collaboratorsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/collaborators{/collaborator}", value = "")
  public String getCollaboratorsUrl() {
    return collaboratorsUrl;
  }

  public void setCollaboratorsUrl(String collaboratorsUrl) {
    this.collaboratorsUrl = collaboratorsUrl;
  }

  public RepositoriesForkUser hooksUrl(String hooksUrl) {
    this.hooksUrl = hooksUrl;
    return this;
  }

   /**
   * Get hooksUrl
   * @return hooksUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/hooks", value = "")
  public String getHooksUrl() {
    return hooksUrl;
  }

  public void setHooksUrl(String hooksUrl) {
    this.hooksUrl = hooksUrl;
  }

  public RepositoriesForkUser branchesUrl(String branchesUrl) {
    this.branchesUrl = branchesUrl;
    return this;
  }

   /**
   * Get branchesUrl
   * @return branchesUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/branches{/branch}", value = "")
  public String getBranchesUrl() {
    return branchesUrl;
  }

  public void setBranchesUrl(String branchesUrl) {
    this.branchesUrl = branchesUrl;
  }

  public RepositoriesForkUser tagsUrl(String tagsUrl) {
    this.tagsUrl = tagsUrl;
    return this;
  }

   /**
   * Get tagsUrl
   * @return tagsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/tags", value = "")
  public String getTagsUrl() {
    return tagsUrl;
  }

  public void setTagsUrl(String tagsUrl) {
    this.tagsUrl = tagsUrl;
  }

  public RepositoriesForkUser blobsUrl(String blobsUrl) {
    this.blobsUrl = blobsUrl;
    return this;
  }

   /**
   * Get blobsUrl
   * @return blobsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/blobs{/sha}", value = "")
  public String getBlobsUrl() {
    return blobsUrl;
  }

  public void setBlobsUrl(String blobsUrl) {
    this.blobsUrl = blobsUrl;
  }

  public RepositoriesForkUser stargazersUrl(String stargazersUrl) {
    this.stargazersUrl = stargazersUrl;
    return this;
  }

   /**
   * Get stargazersUrl
   * @return stargazersUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/stargazers", value = "")
  public String getStargazersUrl() {
    return stargazersUrl;
  }

  public void setStargazersUrl(String stargazersUrl) {
    this.stargazersUrl = stargazersUrl;
  }

  public RepositoriesForkUser contributorsUrl(String contributorsUrl) {
    this.contributorsUrl = contributorsUrl;
    return this;
  }

   /**
   * Get contributorsUrl
   * @return contributorsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/contributors", value = "")
  public String getContributorsUrl() {
    return contributorsUrl;
  }

  public void setContributorsUrl(String contributorsUrl) {
    this.contributorsUrl = contributorsUrl;
  }

  public RepositoriesForkUser commitsUrl(String commitsUrl) {
    this.commitsUrl = commitsUrl;
    return this;
  }

   /**
   * Get commitsUrl
   * @return commitsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/commits{/sha}", value = "")
  public String getCommitsUrl() {
    return commitsUrl;
  }

  public void setCommitsUrl(String commitsUrl) {
    this.commitsUrl = commitsUrl;
  }

  public RepositoriesForkUser commentsUrl(String commentsUrl) {
    this.commentsUrl = commentsUrl;
    return this;
  }

   /**
   * Get commentsUrl
   * @return commentsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/comments{/number}", value = "")
  public String getCommentsUrl() {
    return commentsUrl;
  }

  public void setCommentsUrl(String commentsUrl) {
    this.commentsUrl = commentsUrl;
  }

  public RepositoriesForkUser issueCommentUrl(String issueCommentUrl) {
    this.issueCommentUrl = issueCommentUrl;
    return this;
  }

   /**
   * Get issueCommentUrl
   * @return issueCommentUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/issues/comments{/number}", value = "")
  public String getIssueCommentUrl() {
    return issueCommentUrl;
  }

  public void setIssueCommentUrl(String issueCommentUrl) {
    this.issueCommentUrl = issueCommentUrl;
  }

  public RepositoriesForkUser issuesUrl(String issuesUrl) {
    this.issuesUrl = issuesUrl;
    return this;
  }

   /**
   * Get issuesUrl
   * @return issuesUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/issues{/number}", value = "")
  public String getIssuesUrl() {
    return issuesUrl;
  }

  public void setIssuesUrl(String issuesUrl) {
    this.issuesUrl = issuesUrl;
  }

  public RepositoriesForkUser pullsUrl(String pullsUrl) {
    this.pullsUrl = pullsUrl;
    return this;
  }

   /**
   * Get pullsUrl
   * @return pullsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/pulls{/number}", value = "")
  public String getPullsUrl() {
    return pullsUrl;
  }

  public void setPullsUrl(String pullsUrl) {
    this.pullsUrl = pullsUrl;
  }

  public RepositoriesForkUser milestonesUrl(String milestonesUrl) {
    this.milestonesUrl = milestonesUrl;
    return this;
  }

   /**
   * Get milestonesUrl
   * @return milestonesUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/milestones{/number}", value = "")
  public String getMilestonesUrl() {
    return milestonesUrl;
  }

  public void setMilestonesUrl(String milestonesUrl) {
    this.milestonesUrl = milestonesUrl;
  }

  public RepositoriesForkUser notificationsUrl(String notificationsUrl) {
    this.notificationsUrl = notificationsUrl;
    return this;
  }

   /**
   * Get notificationsUrl
   * @return notificationsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/notifications{?since,all,participating}", value = "")
  public String getNotificationsUrl() {
    return notificationsUrl;
  }

  public void setNotificationsUrl(String notificationsUrl) {
    this.notificationsUrl = notificationsUrl;
  }

  public RepositoriesForkUser labelsUrl(String labelsUrl) {
    this.labelsUrl = labelsUrl;
    return this;
  }

   /**
   * Get labelsUrl
   * @return labelsUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/labels{/name}", value = "")
  public String getLabelsUrl() {
    return labelsUrl;
  }

  public void setLabelsUrl(String labelsUrl) {
    this.labelsUrl = labelsUrl;
  }

  public RepositoriesForkUser releasesUrl(String releasesUrl) {
    this.releasesUrl = releasesUrl;
    return this;
  }

   /**
   * Get releasesUrl
   * @return releasesUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/fnet/gitee-client/releases{/id}", value = "")
  public String getReleasesUrl() {
    return releasesUrl;
  }

  public void setReleasesUrl(String releasesUrl) {
    this.releasesUrl = releasesUrl;
  }

  public RepositoriesForkUser recommend(Boolean recommend) {
    this.recommend = recommend;
    return this;
  }

   /**
   * Get recommend
   * @return recommend
  **/
  @ApiModelProperty(value = "")
  public Boolean isRecommend() {
    return recommend;
  }

  public void setRecommend(Boolean recommend) {
    this.recommend = recommend;
  }

  public RepositoriesForkUser homepage(String homepage) {
    this.homepage = homepage;
    return this;
  }

   /**
   * Get homepage
   * @return homepage
  **/
  @ApiModelProperty(example = "", value = "")
  public String getHomepage() {
    return homepage;
  }

  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }

  public RepositoriesForkUser language(String language) {
    this.language = language;
    return this;
  }

   /**
   * Get language
   * @return language
  **/
  @ApiModelProperty(example = "Java", value = "")
  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public RepositoriesForkUser forksCount(Integer forksCount) {
    this.forksCount = forksCount;
    return this;
  }

   /**
   * Get forksCount
   * @return forksCount
  **/
  @ApiModelProperty(example = "3", value = "")
  public Integer getForksCount() {
    return forksCount;
  }

  public void setForksCount(Integer forksCount) {
    this.forksCount = forksCount;
  }

  public RepositoriesForkUser stargazersCount(Integer stargazersCount) {
    this.stargazersCount = stargazersCount;
    return this;
  }

   /**
   * Get stargazersCount
   * @return stargazersCount
  **/
  @ApiModelProperty(example = "0", value = "")
  public Integer getStargazersCount() {
    return stargazersCount;
  }

  public void setStargazersCount(Integer stargazersCount) {
    this.stargazersCount = stargazersCount;
  }

  public RepositoriesForkUser watchersCount(Integer watchersCount) {
    this.watchersCount = watchersCount;
    return this;
  }

   /**
   * Get watchersCount
   * @return watchersCount
  **/
  @ApiModelProperty(example = "1", value = "")
  public Integer getWatchersCount() {
    return watchersCount;
  }

  public void setWatchersCount(Integer watchersCount) {
    this.watchersCount = watchersCount;
  }

  public RepositoriesForkUser defaultBranch(String defaultBranch) {
    this.defaultBranch = defaultBranch;
    return this;
  }

   /**
   * Get defaultBranch
   * @return defaultBranch
  **/
  @ApiModelProperty(example = "master", value = "")
  public String getDefaultBranch() {
    return defaultBranch;
  }

  public void setDefaultBranch(String defaultBranch) {
    this.defaultBranch = defaultBranch;
  }

  public RepositoriesForkUser openIssuesCount(Integer openIssuesCount) {
    this.openIssuesCount = openIssuesCount;
    return this;
  }

   /**
   * Get openIssuesCount
   * @return openIssuesCount
  **/
  @ApiModelProperty(example = "0", value = "")
  public Integer getOpenIssuesCount() {
    return openIssuesCount;
  }

  public void setOpenIssuesCount(Integer openIssuesCount) {
    this.openIssuesCount = openIssuesCount;
  }

  public RepositoriesForkUser hasIssues(Boolean hasIssues) {
    this.hasIssues = hasIssues;
    return this;
  }

   /**
   * Get hasIssues
   * @return hasIssues
  **/
  @ApiModelProperty(value = "")
  public Boolean isHasIssues() {
    return hasIssues;
  }

  public void setHasIssues(Boolean hasIssues) {
    this.hasIssues = hasIssues;
  }

  public RepositoriesForkUser hasWiki(Boolean hasWiki) {
    this.hasWiki = hasWiki;
    return this;
  }

   /**
   * Get hasWiki
   * @return hasWiki
  **/
  @ApiModelProperty(value = "")
  public Boolean isHasWiki() {
    return hasWiki;
  }

  public void setHasWiki(Boolean hasWiki) {
    this.hasWiki = hasWiki;
  }

  public RepositoriesForkUser pullRequestsEnabled(Boolean pullRequestsEnabled) {
    this.pullRequestsEnabled = pullRequestsEnabled;
    return this;
  }

   /**
   * Get pullRequestsEnabled
   * @return pullRequestsEnabled
  **/
  @ApiModelProperty(value = "")
  public Boolean isPullRequestsEnabled() {
    return pullRequestsEnabled;
  }

  public void setPullRequestsEnabled(Boolean pullRequestsEnabled) {
    this.pullRequestsEnabled = pullRequestsEnabled;
  }

  public RepositoriesForkUser hasPage(Boolean hasPage) {
    this.hasPage = hasPage;
    return this;
  }

   /**
   * Get hasPage
   * @return hasPage
  **/
  @ApiModelProperty(value = "")
  public Boolean isHasPage() {
    return hasPage;
  }

  public void setHasPage(Boolean hasPage) {
    this.hasPage = hasPage;
  }

  public RepositoriesForkUser pushedAt(java.util.Date pushedAt) {
    this.pushedAt = pushedAt;
    return this;
  }

   /**
   * Get pushedAt
   * @return pushedAt
  **/
  @ApiModelProperty(example = "2017-11-15T13:27:53+08:00", value = "")
  public java.util.Date getPushedAt() {
    return pushedAt;
  }

  public void setPushedAt(java.util.Date pushedAt) {
    this.pushedAt = pushedAt;
  }

  public RepositoriesForkUser createdAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(example = "2017-11-15T13:27:53+08:00", value = "")
  public java.util.Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
  }

  public RepositoriesForkUser updatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

   /**
   * Get updatedAt
   * @return updatedAt
  **/
  @ApiModelProperty(example = "2017-11-15T13:27:53+08:00", value = "")
  public java.util.Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(java.util.Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public RepositoriesForkUser parent(RepositoriesForkUserParent parent) {
    this.parent = parent;
    return this;
  }

   /**
   * Get parent
   * @return parent
  **/
  @ApiModelProperty(value = "")
  public RepositoriesForkUserParent getParent() {
    return parent;
  }

  public void setParent(RepositoriesForkUserParent parent) {
    this.parent = parent;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RepositoriesForkUser repositoriesForkUser = (RepositoriesForkUser) o;
    return Objects.equals(this.id, repositoriesForkUser.id) &&
        Objects.equals(this.fullName, repositoriesForkUser.fullName) &&
        Objects.equals(this.url, repositoriesForkUser.url) &&
        Objects.equals(this.path, repositoriesForkUser.path) &&
        Objects.equals(this.name, repositoriesForkUser.name) &&
        Objects.equals(this.owner, repositoriesForkUser.owner) &&
        Objects.equals(this.description, repositoriesForkUser.description) &&
        Objects.equals(this._private, repositoriesForkUser._private) &&
        Objects.equals(this.fork, repositoriesForkUser.fork) &&
        Objects.equals(this.htmlUrl, repositoriesForkUser.htmlUrl) &&
        Objects.equals(this.forksUrl, repositoriesForkUser.forksUrl) &&
        Objects.equals(this.keysUrl, repositoriesForkUser.keysUrl) &&
        Objects.equals(this.collaboratorsUrl, repositoriesForkUser.collaboratorsUrl) &&
        Objects.equals(this.hooksUrl, repositoriesForkUser.hooksUrl) &&
        Objects.equals(this.branchesUrl, repositoriesForkUser.branchesUrl) &&
        Objects.equals(this.tagsUrl, repositoriesForkUser.tagsUrl) &&
        Objects.equals(this.blobsUrl, repositoriesForkUser.blobsUrl) &&
        Objects.equals(this.stargazersUrl, repositoriesForkUser.stargazersUrl) &&
        Objects.equals(this.contributorsUrl, repositoriesForkUser.contributorsUrl) &&
        Objects.equals(this.commitsUrl, repositoriesForkUser.commitsUrl) &&
        Objects.equals(this.commentsUrl, repositoriesForkUser.commentsUrl) &&
        Objects.equals(this.issueCommentUrl, repositoriesForkUser.issueCommentUrl) &&
        Objects.equals(this.issuesUrl, repositoriesForkUser.issuesUrl) &&
        Objects.equals(this.pullsUrl, repositoriesForkUser.pullsUrl) &&
        Objects.equals(this.milestonesUrl, repositoriesForkUser.milestonesUrl) &&
        Objects.equals(this.notificationsUrl, repositoriesForkUser.notificationsUrl) &&
        Objects.equals(this.labelsUrl, repositoriesForkUser.labelsUrl) &&
        Objects.equals(this.releasesUrl, repositoriesForkUser.releasesUrl) &&
        Objects.equals(this.recommend, repositoriesForkUser.recommend) &&
        Objects.equals(this.homepage, repositoriesForkUser.homepage) &&
        Objects.equals(this.language, repositoriesForkUser.language) &&
        Objects.equals(this.forksCount, repositoriesForkUser.forksCount) &&
        Objects.equals(this.stargazersCount, repositoriesForkUser.stargazersCount) &&
        Objects.equals(this.watchersCount, repositoriesForkUser.watchersCount) &&
        Objects.equals(this.defaultBranch, repositoriesForkUser.defaultBranch) &&
        Objects.equals(this.openIssuesCount, repositoriesForkUser.openIssuesCount) &&
        Objects.equals(this.hasIssues, repositoriesForkUser.hasIssues) &&
        Objects.equals(this.hasWiki, repositoriesForkUser.hasWiki) &&
        Objects.equals(this.pullRequestsEnabled, repositoriesForkUser.pullRequestsEnabled) &&
        Objects.equals(this.hasPage, repositoriesForkUser.hasPage) &&
        Objects.equals(this.pushedAt, repositoriesForkUser.pushedAt) &&
        Objects.equals(this.createdAt, repositoriesForkUser.createdAt) &&
        Objects.equals(this.updatedAt, repositoriesForkUser.updatedAt) &&
        Objects.equals(this.parent, repositoriesForkUser.parent);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, fullName, url, path, name, owner, description, _private, fork, htmlUrl, forksUrl, keysUrl, collaboratorsUrl, hooksUrl, branchesUrl, tagsUrl, blobsUrl, stargazersUrl, contributorsUrl, commitsUrl, commentsUrl, issueCommentUrl, issuesUrl, pullsUrl, milestonesUrl, notificationsUrl, labelsUrl, releasesUrl, recommend, homepage, language, forksCount, stargazersCount, watchersCount, defaultBranch, openIssuesCount, hasIssues, hasWiki, pullRequestsEnabled, hasPage, pushedAt, createdAt, updatedAt, parent);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RepositoriesForkUser {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    _private: ").append(toIndentedString(_private)).append("\n");
    sb.append("    fork: ").append(toIndentedString(fork)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    forksUrl: ").append(toIndentedString(forksUrl)).append("\n");
    sb.append("    keysUrl: ").append(toIndentedString(keysUrl)).append("\n");
    sb.append("    collaboratorsUrl: ").append(toIndentedString(collaboratorsUrl)).append("\n");
    sb.append("    hooksUrl: ").append(toIndentedString(hooksUrl)).append("\n");
    sb.append("    branchesUrl: ").append(toIndentedString(branchesUrl)).append("\n");
    sb.append("    tagsUrl: ").append(toIndentedString(tagsUrl)).append("\n");
    sb.append("    blobsUrl: ").append(toIndentedString(blobsUrl)).append("\n");
    sb.append("    stargazersUrl: ").append(toIndentedString(stargazersUrl)).append("\n");
    sb.append("    contributorsUrl: ").append(toIndentedString(contributorsUrl)).append("\n");
    sb.append("    commitsUrl: ").append(toIndentedString(commitsUrl)).append("\n");
    sb.append("    commentsUrl: ").append(toIndentedString(commentsUrl)).append("\n");
    sb.append("    issueCommentUrl: ").append(toIndentedString(issueCommentUrl)).append("\n");
    sb.append("    issuesUrl: ").append(toIndentedString(issuesUrl)).append("\n");
    sb.append("    pullsUrl: ").append(toIndentedString(pullsUrl)).append("\n");
    sb.append("    milestonesUrl: ").append(toIndentedString(milestonesUrl)).append("\n");
    sb.append("    notificationsUrl: ").append(toIndentedString(notificationsUrl)).append("\n");
    sb.append("    labelsUrl: ").append(toIndentedString(labelsUrl)).append("\n");
    sb.append("    releasesUrl: ").append(toIndentedString(releasesUrl)).append("\n");
    sb.append("    recommend: ").append(toIndentedString(recommend)).append("\n");
    sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
    sb.append("    language: ").append(toIndentedString(language)).append("\n");
    sb.append("    forksCount: ").append(toIndentedString(forksCount)).append("\n");
    sb.append("    stargazersCount: ").append(toIndentedString(stargazersCount)).append("\n");
    sb.append("    watchersCount: ").append(toIndentedString(watchersCount)).append("\n");
    sb.append("    defaultBranch: ").append(toIndentedString(defaultBranch)).append("\n");
    sb.append("    openIssuesCount: ").append(toIndentedString(openIssuesCount)).append("\n");
    sb.append("    hasIssues: ").append(toIndentedString(hasIssues)).append("\n");
    sb.append("    hasWiki: ").append(toIndentedString(hasWiki)).append("\n");
    sb.append("    pullRequestsEnabled: ").append(toIndentedString(pullRequestsEnabled)).append("\n");
    sb.append("    hasPage: ").append(toIndentedString(hasPage)).append("\n");
    sb.append("    pushedAt: ").append(toIndentedString(pushedAt)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
    sb.append("    parent: ").append(toIndentedString(parent)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

