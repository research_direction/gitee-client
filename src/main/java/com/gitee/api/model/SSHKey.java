package com.gitee.api.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * this is description
 */
@ApiModel(description = "this is description")

@javax.annotation.Generated(value = "io.swagger.generator.online.JavaClientCodegenImpl", date = "2017-11-16T14:23:50.337+08:00")

public class SSHKey   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("key")
  private String key = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("title")
  private String title = null;

  @SerializedName("created_at")
  private java.util.Date createdAt = null;

  public SSHKey id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "474960", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public SSHKey key(String key) {
    this.key = key;
    return this;
  }

   /**
   * Get key
   * @return key
  **/
  @ApiModelProperty(example = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCXsm3w43Yc9UYCdfKoHjXRZouxnB9YFgD46JNhyOAQhSpR9j/yje7kMNZEeW8UrxcbbD0kHVxA8hTv9J8674HFpA3lEN7PrJwiGkLC0o2vhWTFubavGOWs+krW4GMHWCwLJSSveSytIBVYiQlsJkKtxwab6RYZeuRBfH2i6kYGFwI9sS5vZLUIblH0eQg9m3qWcJF01CU5BO01ZJ6Txx1yRIlEwYd/BX/lfBuqPzPpIbG+APnyJuKhjEoqLk+YiXPebxmhDztT6D4/jbNsXSyUdebvmlcZs13wQfcRHUc6XPa4K8gXbe95UPNXs87qEm7W9vr+zHlUtVr4fxG8Wqb wuyu@bogon", value = "")
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public SSHKey url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/vc-manager-parent/keys/474960", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public SSHKey title(String title) {
    this.title = title;
    return this;
  }

   /**
   * Get title
   * @return title
  **/
  @ApiModelProperty(example = "wuyu@bogon", value = "")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public SSHKey createdAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(example = "2017-09-05T14:53:45+08:00", value = "")
  public java.util.Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SSHKey ssHKey = (SSHKey) o;
    return Objects.equals(this.id, ssHKey.id) &&
        Objects.equals(this.key, ssHKey.key) &&
        Objects.equals(this.url, ssHKey.url) &&
        Objects.equals(this.title, ssHKey.title) &&
        Objects.equals(this.createdAt, ssHKey.createdAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, key, url, title, createdAt);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SSHKey {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    key: ").append(toIndentedString(key)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

