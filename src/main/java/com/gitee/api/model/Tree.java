package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
@ApiModel(description = "this is description")


public class Tree {
    @SerializedName("sha")
    private String sha = null;

    @SerializedName("url")
    private String url = null;

    @SerializedName("tree")

    private java.util.List<Tree> tree = null;

    @SerializedName("truncated")
    private Boolean truncated = null;

    public Tree sha(String sha) {
        this.sha = sha;
        return this;
    }

    /**
     * Get sha
     *
     * @return sha
     **/
    @ApiModelProperty(example = "d9802d8f790d75f14d2a95d5501cb2535ad91fd0", value = "")
    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public Tree url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/gitee-client/git/trees/d9802d8f790d75f14d2a95d5501cb2535ad91fd0", value = "")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Tree tree(java.util.List<Tree> tree) {
        this.tree = tree;
        return this;
    }

    public Tree addTreeItem(Tree treeItem) {
        if (this.tree == null) {
            this.tree = new java.util.ArrayList<Tree>();
        }
        this.tree.add(treeItem);
        return this;
    }

    /**
     * Get tree
     *
     * @return tree
     **/
    @ApiModelProperty(value = "")
    public java.util.List<Tree> getTree() {
        return tree;
    }

    public void setTree(java.util.List<Tree> tree) {
        this.tree = tree;
    }

    public Tree truncated(Boolean truncated) {
        this.truncated = truncated;
        return this;
    }

    /**
     * Get truncated
     *
     * @return truncated
     **/
    @ApiModelProperty(value = "")
    public Boolean isTruncated() {
        return truncated;
    }

    public void setTruncated(Boolean truncated) {
        this.truncated = truncated;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tree tree = (Tree) o;
        return Objects.equals(this.sha, tree.sha) &&
                Objects.equals(this.url, tree.url) &&
                Objects.equals(this.tree, tree.tree) &&
                Objects.equals(this.truncated, tree.truncated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sha, url, tree, truncated);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Tree {\n");

        sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    tree: ").append(toIndentedString(tree)).append("\n");
        sb.append("    truncated: ").append(toIndentedString(truncated)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

