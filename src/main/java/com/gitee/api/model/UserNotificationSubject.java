package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * UserNotificationSubject
 */
public class UserNotificationSubject {
    @SerializedName("title")
    private String title = null;

    @SerializedName("url")
    private String url = null;

    @SerializedName("latest_comment_url")
    private String latestCommentUrl = null;

    @SerializedName("type")
    private String type = null;

    public UserNotificationSubject title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     *
     * @return title
     **/
    @ApiModelProperty(example = "杨蕴琦 推送 1 commit 到 耿继超/zf-data-all 的 master 分支", value = "")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserNotificationSubject url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    @ApiModelProperty(example = "https://gitee.com/api/v5/repos/gjcapp/zf-data-all", value = "")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UserNotificationSubject latestCommentUrl(String latestCommentUrl) {
        this.latestCommentUrl = latestCommentUrl;
        return this;
    }

    /**
     * Get latestCommentUrl
     *
     * @return latestCommentUrl
     **/
    @ApiModelProperty(example = "", value = "")
    public String getLatestCommentUrl() {
        return latestCommentUrl;
    }

    public void setLatestCommentUrl(String latestCommentUrl) {
        this.latestCommentUrl = latestCommentUrl;
    }

    public UserNotificationSubject type(String type) {
        this.type = type;
        return this;
    }

    /**
     * Get type
     *
     * @return type
     **/
    @ApiModelProperty(example = "Project", value = "")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserNotificationSubject userNotificationSubject = (UserNotificationSubject) o;
        return Objects.equals(this.title, userNotificationSubject.title) &&
                Objects.equals(this.url, userNotificationSubject.url) &&
                Objects.equals(this.latestCommentUrl, userNotificationSubject.latestCommentUrl) &&
                Objects.equals(this.type, userNotificationSubject.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, url, latestCommentUrl, type);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserNotificationSubject {\n");

        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    latestCommentUrl: ").append(toIndentedString(latestCommentUrl)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

