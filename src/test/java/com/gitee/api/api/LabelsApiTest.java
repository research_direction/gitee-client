package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Label;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for LabelsApi
 */
public class LabelsApiTest {

    private LabelsApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(LabelsApi.class);
    }

    /**
     * 删除Issue所有标签
     *
     * 删除Issue所有标签
     */
    @Test
    public void deleteV5ReposOwnerRepoIssuesNumberLabelsTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 删除Issue标签
     *
     * 删除Issue标签
     */
    @Test
    public void deleteV5ReposOwnerRepoIssuesNumberLabelsNameTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String name = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, accessToken);

        // TODO: test validations
    }
    /**
     * 删除一个项目标签
     *
     * 删除一个项目标签
     */
    @Test
    public void deleteV5ReposOwnerRepoLabelsNameTest() {
        String owner = null;
        String repo = null;
        String name = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);

        // TODO: test validations
    }
    /**
     * 获取项目Issue的所有标签
     *
     * 获取项目Issue的所有标签
     */
    @Test
    public void getV5ReposOwnerRepoIssuesNumberLabelsTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        // java.util.List<Label> response = api.getV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 获取项目所有标签
     *
     * 获取项目所有标签
     */
    @Test
    public void getV5ReposOwnerRepoLabelsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        // java.util.List<Label> response = api.getV5ReposOwnerRepoLabels(owner, repo, accessToken);

        // TODO: test validations
    }
    /**
     * 根据标签名称获取单个标签
     *
     * 根据标签名称获取单个标签
     */
    @Test
    public void getV5ReposOwnerRepoLabelsNameTest() {
        String owner = null;
        String repo = null;
        String name = null;
        String accessToken = null;
        // Label response = api.getV5ReposOwnerRepoLabelsName(owner, repo, name, accessToken);

        // TODO: test validations
    }
    /**
     * 更新一个项目标签
     *
     * 更新一个项目标签
     */
    @Test
    public void patchV5ReposOwnerRepoLabelsOriginalNameTest() {
        String owner = null;
        String repo = null;
        Integer originalName = null;
        String accessToken = null;
        String name = null;
        String color = null;
        // Label response = api.patchV5ReposOwnerRepoLabelsOriginalName(owner, repo, originalName, accessToken, name, color);

        // TODO: test validations
    }
    /**
     * 创建Issue标签
     *
     * 创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]
     */
    @Test
    public void postV5ReposOwnerRepoIssuesNumberLabelsTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        // Label response = api.postV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 创建项目标签
     *
     * 创建项目标签
     */
    @Test
    public void postV5ReposOwnerRepoLabelsTest() {
        String owner = null;
        String repo = null;
        String name = null;
        String color = null;
        String accessToken = null;
        // Label response = api.postV5ReposOwnerRepoLabels(owner, repo, name, color, accessToken);

        // TODO: test validations
    }
    /**
     * 替换Issue所有标签
     *
     * 替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\&quot;performance\&quot;, \&quot;bug\&quot;]
     */
    @Test
    public void putV5ReposOwnerRepoIssuesNumberLabelsTest() {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        // Label response = api.putV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, accessToken);

        // TODO: test validations
    }
}
