package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Milestone;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for MilestonesApi
 */
public class MilestonesApiTest {

    private MilestonesApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(MilestonesApi.class);
    }

    /**
     * 删除项目单个里程碑
     *
     * 删除项目单个里程碑
     */
    @Test
    public void deleteV5ReposOwnerRepoMilestonesNumberTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 获取项目所有里程碑
     *
     * 获取项目所有里程碑
     */
    @Test
    public void getV5ReposOwnerRepoMilestonesTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String state = null;
        String sort = null;
        String direction = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Milestone> response = api.getV5ReposOwnerRepoMilestones(owner, repo, accessToken, state, sort, direction, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取项目单个里程碑
     *
     * 获取项目单个里程碑
     */
    @Test
    public void getV5ReposOwnerRepoMilestonesNumberTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        // Milestone response = api.getV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 更新项目里程碑
     *
     * 更新项目里程碑
     */
    @Test
    public void patchV5ReposOwnerRepoMilestonesNumberTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String title = null;
        String accessToken = null;
        String state = null;
        String description = null;
        String dueOn = null;
        // Milestone response = api.patchV5ReposOwnerRepoMilestonesNumber(owner, repo, number, title, accessToken, state, description, dueOn);

        // TODO: test validations
    }
    /**
     * 创建项目里程碑
     *
     * 创建项目里程碑
     */
    @Test
    public void postV5ReposOwnerRepoMilestonesTest() {
        String owner = null;
        String repo = null;
        String title = null;
        String accessToken = null;
        String state = null;
        String description = null;
        String dueOn = null;
        // Milestone response = api.postV5ReposOwnerRepoMilestones(owner, repo, title, accessToken, state, description, dueOn);

        // TODO: test validations
    }
}
