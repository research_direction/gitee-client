package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.PullRequest;
import com.gitee.api.model.PullRequestComments;
import com.gitee.api.model.PullRequestCommits;
import com.gitee.api.model.PullRequestFiles;
import com.gitee.api.model.UserBasic;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for PullRequestsApi
 */
public class PullRequestsApiTest {

    private PullRequestsApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(PullRequestsApi.class);
    }

    /**
     * 删除评论
     *
     * 删除评论
     */
    @Test
    public void deleteV5ReposOwnerRepoPullsCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 移除审查人员
     *
     * 移除审查人员
     */
    @Test
    public void deleteV5ReposOwnerRepoPullsNumberRequestedReviewersTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        java.util.List<String> reviewers = null;
        String accessToken = null;
        // Void response = api.deleteV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, accessToken);

        // TODO: test validations
    }
    /**
     * 获取Pull Request列表
     *
     * 获取Pull Request列表
     */
    @Test
    public void getV5ReposOwnerRepoPullsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String state = null;
        String head = null;
        String base = null;
        String sort = null;
        String direction = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<PullRequest> response = api.getV5ReposOwnerRepoPulls(owner, repo, accessToken, state, head, base, sort, direction, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取该项目下的所有Pull Request评论
     *
     * 获取该项目下的所有Pull Request评论
     */
    @Test
    public void getV5ReposOwnerRepoPullsCommentsTest() {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<PullRequestComments> response = api.getV5ReposOwnerRepoPullsComments(owner, repo, accessToken, sort, direction, since, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取Pull Request的某个评论
     *
     * 获取Pull Request的某个评论
     */
    @Test
    public void getV5ReposOwnerRepoPullsCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        // PullRequestComments response = api.getV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    /**
     * 获取单个Pull Request
     *
     * 获取单个Pull Request
     */
    @Test
    public void getV5ReposOwnerRepoPullsNumberTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        // PullRequest response = api.getV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 获取某个Pull Request的所有评论
     *
     * 获取某个Pull Request的所有评论
     */
    @Test
    public void getV5ReposOwnerRepoPullsNumberCommentsTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<PullRequestComments> response = api.getV5ReposOwnerRepoPullsNumberComments(owner, repo, number, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取某Pull Request的所有Commit信息。最多显示250条Commit
     *
     * 获取某Pull Request的所有Commit信息。最多显示250条Commit
     */
    @Test
    public void getV5ReposOwnerRepoPullsNumberCommitsTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        // java.util.List<PullRequestCommits> response = api.getV5ReposOwnerRepoPullsNumberCommits(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * Pull Request Commit文件列表。最多显示300条diff
     *
     * Pull Request Commit文件列表。最多显示300条diff
     */
    @Test
    public void getV5ReposOwnerRepoPullsNumberFilesTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        // java.util.List<PullRequestFiles> response = api.getV5ReposOwnerRepoPullsNumberFiles(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 判断Pull Request是否已经合并
     *
     * 判断Pull Request是否已经合并
     */
    @Test
    public void getV5ReposOwnerRepoPullsNumberMergeTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        // Void response = api.getV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken);

        // TODO: test validations
    }
    /**
     * 获取审查人员的列表
     *
     * 获取审查人员的列表
     */
    @Test
    public void getV5ReposOwnerRepoPullsNumberRequestedReviewersTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // UserBasic response = api.getV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 编辑评论
     *
     * 编辑评论
     */
    @Test
    public void patchV5ReposOwnerRepoPullsCommentsIdTest() {
        String owner = null;
        String repo = null;
        Integer id = null;
        String body = null;
        String accessToken = null;
        // PullRequestComments response = api.patchV5ReposOwnerRepoPullsCommentsId(owner, repo, id, body, accessToken);

        // TODO: test validations
    }
    /**
     * 更新Pull Request信息
     *
     * 更新Pull Request信息
     */
    @Test
    public void patchV5ReposOwnerRepoPullsNumberTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        String title = null;
        String body = null;
        String state = null;
        // PullRequest response = api.patchV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken, title, body, state);

        // TODO: test validations
    }
    /**
     * 创建Pull Request
     *
     * 创建Pull Request
     */
    @Test
    public void postV5ReposOwnerRepoPullsTest() {
        String owner = null;
        String repo = null;
        String title = null;
        String head = null;
        String base = null;
        String accessToken = null;
        String body = null;
        String issue = null;
        // PullRequest response = api.postV5ReposOwnerRepoPulls(owner, repo, title, head, base, accessToken, body, issue);

        // TODO: test validations
    }
    /**
     * 提交Pull Request评论
     *
     * 提交Pull Request评论
     */
    @Test
    public void postV5ReposOwnerRepoPullsNumberCommentsTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String body = null;
        String accessToken = null;
        String commitId = null;
        String path = null;
        Integer position = null;
        // PullRequestComments response = api.postV5ReposOwnerRepoPullsNumberComments(owner, repo, number, body, accessToken, commitId, path, position);

        // TODO: test validations
    }
    /**
     * 增加审查人员
     *
     * 增加审查人员
     */
    @Test
    public void postV5ReposOwnerRepoPullsNumberRequestedReviewersTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        java.util.List<String> reviewers = null;
        String accessToken = null;
        // PullRequest response = api.postV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, accessToken);

        // TODO: test validations
    }
    /**
     * 合并Pull Request
     *
     * 合并Pull Request
     */
    @Test
    public void putV5ReposOwnerRepoPullsNumberMergeTest() {
        String owner = null;
        String repo = null;
        Integer number = null;
        String accessToken = null;
        // Void response = api.putV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken);

        // TODO: test validations
    }
}
